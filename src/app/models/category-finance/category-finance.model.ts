export class ICategoryFinance {
  _id ?: string;
  hash_identificacao?: string ;
  titulo?: string;
  descricao?: string;
  fixo?: boolean;
  permite_despesa?: boolean;
  habilitado?: boolean;
  criado_por?: string;
}
