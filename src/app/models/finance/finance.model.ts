import { ICategoryFinance } from "../category-finance/category-finance.model";

export class IFinance {
  _id ?: string;
  categoria?: ICategoryFinance;
  titulo?: string;
  descricao?: string;
  valor?: string;
  despesa?: boolean;
  criado_por?: string;
}

export class IFinanceResume {
  balance ?: string;
  monthly_expenses ?: string;
  savings_value ?: string;
  monthly_value ?: string;
}
