import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


// Models
import { IFinance, IFinanceResume } from '../../models/finance/finance.model';


@Injectable({
  providedIn: 'root'
})
export class FinanceService {
  _url = `${environment.serverUrl}`;

  public financeListSubject$ = new BehaviorSubject<IFinance[]>([]);
  public financeResumeSubject$ = new BehaviorSubject<IFinanceResume>({});
  public carregado: boolean = false;

  constructor(private http: HttpClient) { }

  public getFinanceList(): Observable<IFinance[]> {
    let retFinanceList = new Observable<IFinance[]>();
    if (!this.carregado) {
      retFinanceList = this.http.get<IFinance[]>(`${this._url}/financas`)
        .pipe(
          tap((financeList: IFinance[]) => {
            this.financeListSubject$.next(financeList);
            this.carregado = true;
          })
        );
    }
    return retFinanceList;
  }

  public getFinanceResume(): Observable<IFinanceResume> {
    let retFinanceResume = new Observable<IFinanceResume>();
    if (!this.carregado) {
      retFinanceResume = this.http.get<IFinanceResume>(`${this._url}/financas-resume`)
        .pipe(
          tap((financeResume: IFinanceResume) => {
            this.financeResumeSubject$.next(financeResume);
            this.carregado = true;
          })
        );
    }
    return retFinanceResume;
  }

  public postFinance(financeData: IFinance): Observable<any> {
    const urlPostFinanceFinance = `${this._url}/financa`
    return this.http.post(urlPostFinanceFinance, financeData)
            .pipe(
              tap((finance: IFinance) => {
                this.financeListSubject$.value.push(finance);
                this.financeListSubject$.next(this.financeListSubject$.value);
              })
            );
  }

}
