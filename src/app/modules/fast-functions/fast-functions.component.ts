import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'home-fast-functions',
  templateUrl: './fast-functions.component.html',
  styleUrls: ['./fast-functions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FastFunctionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
