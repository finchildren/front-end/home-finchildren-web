import { Component, OnInit } from '@angular/core';
import { IFinanceResume } from 'src/app/models/finance/finance.model';
import { FinanceService } from 'src/app/services/finance/finance.service';


@Component({
  selector: 'home-resume-finance',
  templateUrl: './resume-finance.component.html',
  styleUrls: ['./resume-finance.component.scss']
})
export class ResumeFinanceComponent implements OnInit {

  financeResume: IFinanceResume = {};

  constructor(private financeService: FinanceService) {
    this._getFinanceResume();
  }

  ngOnInit() {
  }

  private _getFinanceResume(): void{
    this.financeService.getFinanceResume().subscribe((fResume: IFinanceResume) => {
      this.financeResume = fResume;
    });
  }

}
