import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

export interface UserData {
  id: string;
  item: string;
  categoria: string;
  valor: string;
}

/** Constants used to fill up our data base. */
const CATEGORIA: string[] = [
  'Lazer',
  'Comida',
  'Gasto fixo',
  'Vestimenta',
  'Eletronicos',
  'Brinquedos',
  'Viagens',
  'Higiene',
];
const ITEMS: string[] = [
  'Fralda XG',
  'Brinquedo de sereia',
  'Figurinhas da copa',
  'Valor poupança',
  'Escola filha',
  'Escola filho',
  'Brinquedo aleatório',
  'Tv nova',
  'Passeio',
  'Passeio filha',
  'Notebook',
  'Viagem de acampamento',
  'Material escolar',
  'Roupas filha',
  'Roupa filho',
  'fast-food',
  'Passeio no parque',
  'Uber',
  'Milho de rua',
];


@Component({
  selector: 'home-content-list-finance',
  templateUrl: './content-list-finance.component.html',
  styleUrls: ['./content-list-finance.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentListFinanceComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'item', 'categoria', 'valor'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor() {
    // Create 100 users
    const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const item =
    ITEMS[Math.round(Math.random() * (ITEMS.length - 1))] +
    ' ' +
    ITEMS[Math.round(Math.random() * (ITEMS.length - 1))].charAt(0) +
    '.';

  return {
    id: id.toString(),
    item: item,
    categoria: CATEGORIA[Math.round(Math.random() * (CATEGORIA.length - 1))],
    valor: Math.round(Math.random() * 100).toString(),
  };
}
