import { Component, OnInit } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
@Component({
  selector: 'home-chart-spending',
  templateUrl: './chart-spending.component.html',
  styleUrls: ['./chart-spending.component.scss']
})
export class ChartSpendingComponent implements OnInit {

  imageUrl = assetUrl('img/chart-spending.png');

  constructor() { }

  ngOnInit() {
  }

}
