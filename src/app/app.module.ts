import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BreadcrumbComponent } from './modules/breadcrumb/breadcrumb.component';
import { ResumeFinanceComponent } from './modules/resume-finance/resume-finance.component';
import { ListFinanceComponent } from './modules/list-finance/list-finance.component';
import { ContentListFinanceComponent } from './modules/list-finance/content-list-finance/content-list-finance.component';
import { ChartSpendingComponent } from './modules/chart-spending/chart-spending.component';
import { EventsCalendarComponent } from './modules/events-calendar/events-calendar.component';
import { FastFunctionsComponent } from './modules/fast-functions/fast-functions.component';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BreadcrumbComponent,
    ResumeFinanceComponent,
    ListFinanceComponent,
    ContentListFinanceComponent,
    ChartSpendingComponent,
    EventsCalendarComponent,
    FastFunctionsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
